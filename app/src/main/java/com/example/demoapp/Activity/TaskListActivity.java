package com.example.demoapp.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.demoapp.R;
import com.example.demoapp.Adapter.TaskAdapter;
import com.example.demoapp.Model.TaskModel;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TaskListActivity extends AppCompatActivity {
    TaskAdapter adapter;
    private List<TaskModel> listData;
    private RecyclerView rv;
    private TextView tvEmptytask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_list);

        tvEmptytask = findViewById(R.id.tvEmptytask);
        rv = findViewById(R.id.recyclerView);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        listData=new ArrayList<>();


        final DatabaseReference nm= FirebaseDatabase.getInstance().getReference("TaskList");
        String i = String.valueOf(getIntent().getExtras().get("emil"));

        nm.orderByChild("email").equalTo(i).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for(DataSnapshot datas: dataSnapshot.getChildren()){
                    TaskModel l=datas.getValue(TaskModel.class);
                    listData.add(l);
                    if(l!=null){
                        tvEmptytask.setVisibility(View.GONE);
                    }else {
                        tvEmptytask.setVisibility(View.VISIBLE);
                    }
                }
                adapter=new TaskAdapter(listData, TaskListActivity.this);
                rv.setAdapter(adapter);

            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });

    }

}