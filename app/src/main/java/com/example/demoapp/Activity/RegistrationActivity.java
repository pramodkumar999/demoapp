package com.example.demoapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import com.example.demoapp.R;
import com.example.demoapp.Model.UserModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputLayout;

import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class RegistrationActivity extends AppCompatActivity {
    private EditText Username, email, phone, password, age;
    TextInputLayout nameError, emailError, phoneError, passError, ageError;
    private Button btnRegister;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    UserModel userModel;
    private TextView logintext;
    boolean isNameValid, isEmailValid, isPhoneValid, isPasswordValid, isAgeValid;
    FirebaseAuth firebaseAuth;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registration_activity);
        Username = findViewById(R.id.Username);
        email = findViewById(R.id.email);
        phone = findViewById(R.id.phone);
        password = findViewById(R.id.password);
        age = findViewById(R.id.age);


        logintext = (TextView) findViewById(R.id.login);
        nameError = (TextInputLayout) findViewById(R.id.nameError);
        emailError = (TextInputLayout) findViewById(R.id.emailError);
        phoneError = (TextInputLayout) findViewById(R.id.phoneError);
        passError = (TextInputLayout) findViewById(R.id.passError);
        ageError = (TextInputLayout) findViewById(R.id.ageError);


        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("UsersData");
        firebaseAuth = FirebaseAuth.getInstance();

        pref = getSharedPreferences("user_details",MODE_PRIVATE);
        Intent intent = new Intent(RegistrationActivity.this, DashboardActivity.class);
        if(pref.contains("emil") && pref.contains("password")){
            startActivity(intent);
        }


        userModel = new UserModel();

        logintext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(RegistrationActivity.this, LoginActivity.class);
                startActivity(i);
            }
        });

        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SetValidation();
            }
        });
    }



    private void SetValidation() {
        if (Username.getText().toString().isEmpty()) {
            nameError.setError(getResources().getString(R.string.name_error));
            isNameValid = false;
        } else  {
            isNameValid = true;
            nameError.setErrorEnabled(false);
        }

        if (email.getText().toString().isEmpty()) {
            emailError.setError(getResources().getString(R.string.email_error));
            isEmailValid = false;
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString()).matches()) {
            emailError.setError(getResources().getString(R.string.error_invalid_email));
            isEmailValid = false;
        } else  {
            isEmailValid = true;
            emailError.setErrorEnabled(false);
        }

        //age
        if (age.getText().toString().isEmpty()) {
            ageError.setError(getResources().getString(R.string.ageError));
            isAgeValid = false;
        }  else  {
            isAgeValid = true;
            emailError.setErrorEnabled(false);
        }

        // Check for a valid phone number.
        if (phone.getText().toString().isEmpty()) {
            phoneError.setError(getResources().getString(R.string.phone_error));
            isPhoneValid = false;
        } else  {
            isPhoneValid = true;
            phoneError.setErrorEnabled(false);
        }

        // Check for a valid password.
        if (password.getText().toString().isEmpty()) {
            passError.setError(getResources().getString(R.string.password_error));
            isPasswordValid = false;
        } else if (password.getText().length() < 6) {
            passError.setError(getResources().getString(R.string.error_invalid_password));
            isPasswordValid = false;
        } else  {
            isPasswordValid = true;
            passError.setErrorEnabled(false);
        }

        if (isNameValid && isEmailValid && isPhoneValid && isPasswordValid) {

            String name_ = Username.getText().toString();
            String emil_ = email.getText().toString();
            String phone_ = phone.getText().toString();
            String password_ = password.getText().toString();
            String age_ = age.getText().toString();

                addDatatoFirebase(name_, emil_, phone_,password_, age_);
        }
    }

    private void addDatatoFirebase(String nm, String em, String ph,String pa,String ag) {
        userModel.setUsername(nm);
        userModel.setEmail(em);
        userModel.setPhone(ph);
        userModel.setAge(ag);
        userModel.setPassword(pa);

        firebaseAuth.createUserWithEmailAndPassword(em, pa).addOnCompleteListener(RegistrationActivity.this, new OnCompleteListener() {
            @Override
            public void onComplete(@NonNull Task task) {

                if (!task.isSuccessful()) {
                    Toast.makeText(RegistrationActivity.this.getApplicationContext(),
                            "SignUp unsuccessful: " + task.getException().getMessage(),
                            Toast.LENGTH_SHORT).show();
                } else {
                    databaseReference.addValueEventListener(new ValueEventListener() {
                        @Override
                        public void onDataChange(@NonNull DataSnapshot snapshot) {
                            databaseReference.child("users").child(ph).setValue(userModel);
                            Toast.makeText(RegistrationActivity.this, "Successfully Registered ", Toast.LENGTH_SHORT).show();
                            SharedPreferences.Editor editor = pref.edit();
                            editor.putString("emil",em);
                            editor.putString("password",pa);
                            editor.commit();

                            Intent i = new Intent(RegistrationActivity.this, DashboardActivity.class);
                            startActivity(i);

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError error) {
                            Toast.makeText(RegistrationActivity.this, "Registration Failed " + error, Toast.LENGTH_SHORT).show();
                        }
                    });

                }
            }
        });
    }


}