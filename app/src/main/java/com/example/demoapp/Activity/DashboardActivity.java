package com.example.demoapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.example.demoapp.R;
import com.example.demoapp.Model.UserModel;
import com.example.demoapp.Adapter.UsersAdapter;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, BottomNavigationView.OnNavigationItemSelectedListener {

    UsersAdapter adapter;
    private List<UserModel> listData;
    private List<UserModel> listDataNew;
    private RecyclerView rv;
    SharedPreferences prf;
    public BottomNavigationView navigationView;
    FloatingActionButton fab;
    boolean isTheme;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        navigationView =  findViewById(R.id.bottomNavigationView);
        fab =  findViewById(R.id.fab);
        navigationView.setOnNavigationItemSelectedListener(this);
        prf = getSharedPreferences("user_details",MODE_PRIVATE);
        rv = findViewById(R.id.recyclerView);
        rv.setHasFixedSize(true);
        rv.setLayoutManager(new LinearLayoutManager(this));
        listData=new ArrayList<>();
        listDataNew=new ArrayList<>();


        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i1 = new Intent(DashboardActivity.this, TaskActivity.class);
                startActivity(i1);
            }
        });

        final DatabaseReference nm= FirebaseDatabase.getInstance().getReference("UsersData/users");
        nm.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()){
                    for (DataSnapshot npsnapshot : dataSnapshot.getChildren()){
                        UserModel l=npsnapshot.getValue(UserModel.class);
                        listData.add(l);


                    }

                    for(int i =0; i<listData.size(); i ++){
                        String emil = prf.getString("emil","");
                        if(emil.equals(listData.get(i).getEmail())){

                            listDataNew.add(0,listData.get(i));
                        }else {
                            listDataNew.add(listData.get(i));
                        }
                    }

                    adapter=new UsersAdapter(listDataNew, DashboardActivity.this);
                    rv.setAdapter(adapter);

                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    public void onBackPressed() {
      //  super.onBackPressed();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finishAffinity();
                        System.exit(0);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull @NotNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.logout:
                SharedPreferences.Editor editor = prf.edit();
                editor.clear();
                editor.commit();
                Intent i = new Intent(DashboardActivity.this, LoginActivity.class);
                startActivity(i);
                break;

            case R.id.placeholder:
                Intent i1 = new Intent(DashboardActivity.this,TaskActivity.class);
                startActivity(i1);
                break;

                case R.id.home:

                    if(isTheme) {
                        isTheme = false;
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    }else {
                        isTheme = true;
                        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    }
                break;

        }
        return false;
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
