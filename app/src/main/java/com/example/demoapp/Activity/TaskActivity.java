package com.example.demoapp.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.demoapp.R;
import com.example.demoapp.Model.TaskModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

public class TaskActivity extends AppCompatActivity {
    private Button addTask;
    private EditText etTask, etDesc;
    FirebaseAuth firebaseAuth;
    FirebaseDatabase firebaseDatabase;
    DatabaseReference databaseReference;
    SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);
        addTask = findViewById(R.id.addTask);
        etTask = findViewById(R.id.etTask);
        etDesc = findViewById(R.id.etDesc);

        firebaseDatabase = FirebaseDatabase.getInstance();
        databaseReference = firebaseDatabase.getReference("TaskList");
        firebaseAuth = FirebaseAuth.getInstance();

        addTask.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String task = etTask.getText().toString();
                String desc = etDesc.getText().toString();


                SharedPreferences  pref = getSharedPreferences("user_details",MODE_PRIVATE);
                String email = pref.getString("emil", null);

                TaskModel t = new TaskModel();
                t.setTask(task);
                t.setDesc(desc);
                t.setEmail(email);

                databaseReference.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(@NonNull DataSnapshot snapshot) {
                        databaseReference.child(task).setValue(t);
                        Toast.makeText(TaskActivity.this, "Successfully Added task ", Toast.LENGTH_SHORT).show();
                        etTask.setText("");
                        etDesc.setText("");
                    }

                    @Override
                    public void onCancelled(@NonNull @NotNull DatabaseError error) {

                    }
                });
            }
        });
    }
}