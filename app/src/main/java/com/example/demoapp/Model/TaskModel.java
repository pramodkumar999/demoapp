package com.example.demoapp.Model;

import java.util.Map;

public class TaskModel {
    private String task;
    private String desc;
    private String email;

    public TaskModel() {
        this.task = task;
        this.desc = desc;
        this.email = email;
    }

    public String getTask() {
        return task;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


}
