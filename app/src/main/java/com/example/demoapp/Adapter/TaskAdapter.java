package com.example.demoapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.demoapp.R;
import com.example.demoapp.Model.TaskModel;

import java.util.List;

public class TaskAdapter extends  RecyclerView.Adapter<TaskAdapter.ViewHolder>{
    private static final int MODE_PRIVATE = 1;
    private List<TaskModel> listData;
    private Context context;

    public TaskAdapter(List<TaskModel> listData, Context context) {
        this.listData = listData;
        this.context = context;
    }

    @NonNull
    @Override
    public TaskAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_item_task,parent,false);
        return new TaskAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskAdapter.ViewHolder holder, int position) {
        TaskModel model=listData.get(position);
        holder.tvTask.setText(model.getTask());
        holder.tvDesc.setText(model.getDesc());

    }

    @Override
    public int getItemCount() {
        return listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        TextView tvTask, tvDesc;
        public ViewHolder(View itemView) {
            super(itemView);
            tvTask = itemView.findViewById(R.id.tvTask);
            tvDesc = itemView.findViewById(R.id.tvDesc);

        }
    }

}
