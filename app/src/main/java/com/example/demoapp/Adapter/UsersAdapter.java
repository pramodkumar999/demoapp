package com.example.demoapp.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.demoapp.Activity.TaskListActivity;
import com.example.demoapp.R;
import com.example.demoapp.Model.UserModel;

import java.util.List;

public class UsersAdapter extends  RecyclerView.Adapter<UsersAdapter.ViewHolder>{
    private static final int MODE_PRIVATE = 1;
    private List<UserModel>listData;
    private Context context;

    public UsersAdapter(List<UserModel> listData, Context context) {
        this.listData = listData;
        this.context = context;
    }

    @NonNull
@Override
public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.cardview_item,parent,false);
        return new ViewHolder(view);
        }

@Override
public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        UserModel model=listData.get(position);
    holder.tvName.setText(model.getUsername());
    holder.tvAge.setText(model.getAge());
    holder.tvEmial.setText(model.getEmail());
    holder.tvMobile.setText(model.getPhone());

    holder.cardView.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(context, TaskListActivity.class);
            i.putExtra("emil",model.getEmail());
            context.startActivity(i);

        }
    });

    @SuppressLint("WrongConstant")
    SharedPreferences mPreferences = context.getSharedPreferences("user_details",MODE_PRIVATE);
    String emil = mPreferences.getString("emil","");

    if(model.getEmail().equals(emil)) {
    ((CardView)holder.cardView).setCardBackgroundColor(Color.parseColor("#989494"));
    }
    }




    @Override
public int getItemCount() {
        return listData.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder{
    TextView tvName, tvAge, tvEmial, tvMobile;
    CardView cardView;
    public ViewHolder(View itemView) {
        super(itemView);
        tvName = itemView.findViewById(R.id.tvName);
        tvAge = itemView.findViewById(R.id.tvAge);
        tvEmial = itemView.findViewById(R.id.tvEmial);
        tvMobile = itemView.findViewById(R.id.tvMobile);
        cardView = itemView.findViewById(R.id.maincardview);
    }
}

}
